<p align="center">
  <img
    width="400"
    src="https://gitlab.com/daomah/vaporterm/-/raw/68687638b4d866c7f113d4ce59503169e1992213/logo.png"
    alt="v a p o r t e r m"
  />
</p>

a simple, vaporwavey oh-my-posh theme for your terminal

<p align="center">
  <img
    width="400"
    src="https://gitlab.com/daomah/vaporterm/-/raw/main/images/vt-screenshot.png"
    alt="v a p o r t e r m"
  />
</p>

## Installation
1. install [omp](https://ohmyposh.dev/docs/)
2. download [vaporterm.omp.json](https://gitlab.com/daomah/vaporterm/-/blob/4dd8ea36649019da0fdbbee4cf22442b69bb245f/vaporterm.omp.json)
3. [point](https://ohmyposh.dev/docs/installation/customize) to theme

## Thanks
- [Jan](https://github.com/JanDeDobbeleer) for creating omp
- Wozzy for helping me get the Powershell version number to truncate to 3 chars
- Greenquee for the [palette](https://www.color-hex.com/color-palette/10221)

## License
[gnu gpl v3](https://gitlab.com/daomah/vaporterm/-/blob/main/LICENSE)